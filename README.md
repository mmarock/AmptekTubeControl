
AmptekTubeControl
=================

This implementation is inspired by the given source code
of Ampteks MiniX-tube for Windows.<br>
To build this you need Qt5, Cmake and libftdi installed.<br>

Since these libs are portable the software should work on windows too.<br>
If there are multiple tubes connected, a selection can be made by
supplying the serial number as commandline argument.
If there is no serial number, the first device is used.
