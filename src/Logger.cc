

/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Logger.hh"

#include <cassert>
#include <cstdarg>
#include <cstdio>

#include <syslog.h>

namespace logger
{

////////////////////////////////////////
//      private non member
////////////////////////////////////////
static struct Data {
	enum VERBOSITY verbosity;

	Data() : verbosity(VERBOSITY::V_INFO)
	{
		openlog("AmptekTubeControl", LOG_CONS | LOG_PID, LOG_USER);
	}

	~Data() { closelog(); }

} data_;

static const char *verbose_str(const enum VERBOSITY &v)
{
	switch (v) {
	case VERBOSITY::V_EMERG:
		return "emerg";
	case VERBOSITY::V_ALERT:
		return "alert";
	case VERBOSITY::V_CRIT:
		return "crit";
	case VERBOSITY::V_ERR:
		return "err";
	case VERBOSITY::V_WARNING:
		return "warning";
	case VERBOSITY::V_NOTICE:
		return "notice";
	case VERBOSITY::V_INFO:
		return "info";
	case VERBOSITY::V_DEBUG:
		return "debug";
	};
}

static int priority(const enum VERBOSITY &v)
{
	switch (v) {
	case VERBOSITY::V_EMERG:
		return LOG_EMERG;
	case VERBOSITY::V_ALERT:
		return LOG_ALERT;
	case VERBOSITY::V_CRIT:
		return LOG_CRIT;
	case VERBOSITY::V_ERR:
		return LOG_ERR;
	case VERBOSITY::V_WARNING:
		return LOG_WARNING;
	case VERBOSITY::V_NOTICE:
		return LOG_NOTICE;
	case VERBOSITY::V_INFO:
		return LOG_INFO;
	case VERBOSITY::V_DEBUG:
		return LOG_DEBUG;
	};
}

////////////////////////////////////////
//      public non member
////////////////////////////////////////

void set_verbosity(enum VERBOSITY v) {
    data_.verbosity = v;
    logger::write(logger::V_DEBUG, "logger: set to %s verbosity", verbose_str(v));
}

const enum VERBOSITY &verbosity() { return data_.verbosity; }

void write(const enum VERBOSITY &v, const char *fmt, ...)
{
    assert(fmt && "fmt is NULL");
	if (data_.verbosity < v) {
		return;
	}

	va_list args;
	char buffer[1024];
	snprintf(buffer, sizeof(buffer), "[%s] %s", verbose_str(v), fmt);

	va_start(args, fmt);
	vsyslog(priority(v), buffer, args);
	va_end(args);
}

} // namespace logger
