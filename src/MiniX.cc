/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "MiniX.hh"

#include <ftdi.h>

#include <cassert>

#include "Logger.hh"

namespace amptek
{

/**
 * @page MiniXIntro Using the MiniX library
 *
 * This library is used by querying devices
 * and then moving one into the MiniX Constructor.<br>
 * Since the underlying library uses libusb this
 * should work with all systems where libusb can be found.<br>
 *
 * There is an dependency on syslog.h in Logger.cc,
 * so on windows something like the mingw compiler should
 * be used or just modify the Logger module
 * to use stdout instead of the syslog.
 *
 * */

////////////////////////////////////////
//      class MiniXException
////////////////////////////////////////

MiniXException::MiniXException(const char *err)
    : msg_(std::string("MiniX: ") + err)
{
}

MiniXException::MiniXException(const std::string &err)
    : msg_(std::string("MiniX: ") + err)
{
}

const char *MiniXException::what() const noexcept { return msg_.c_str(); }

////////////////////////////////////////
//      class InputError
////////////////////////////////////////

InputError::InputError(const char *err) : MiniXException(err) {}

InputError::InputError(const std::string &err) : MiniXException(err) {}

////////////////////////////////////////
//      class CriticalError
////////////////////////////////////////

CriticalError::CriticalError(const char *err)
    : MiniXException(std::string("Critical: ") + err)
{
}

CriticalError::CriticalError(const std::string &err)
    : MiniXException(std::string("Critical: ") + err)
{
}

////////////////////////////////////////
//      struct FtdiError
////////////////////////////////////////

struct FtdiError : public CriticalError {

	enum FtdiFunction : uint8_t {
		UsbFindAll,
		UsbOpenDev,
		UsbGetStrings,
		SetLatencyTimer,
		SetBitmode,
		WriteData,
		ReadData
	};

	FtdiError(enum FtdiFunction f, const struct ftdi_context *ctx);

private:
	static const char *function_str(FtdiFunction f);
};

FtdiError::FtdiError(enum FtdiFunction f, const struct ftdi_context *ctx)
    : CriticalError(
	  std::string("Ftdi: ") + function_str(f) + ": " +
	  ftdi_get_error_string(const_cast<struct ftdi_context *>(ctx)))
{
}

const char *FtdiError::function_str(FtdiFunction f)
{
	switch (f) {
	case UsbFindAll:
		return "fetdi_usb_find_all()";
	case UsbOpenDev:
		return "frtdi_usb_open_dev()";
	case UsbGetStrings:
		return "ftdi_usb_get_strings()";
	case SetLatencyTimer:
		return "ftdi_set_latency_timer()";
	case SetBitmode:
		return "ftdi_set_bitmode()";
	case WriteData:
		return "ftdi_write_data()";
	case ReadData:
		return "ftdi_read_data()";
	};
	assert(0 && "should never get here");
	return nullptr;
}

////////////////////////////////////////
//      struct InvalidSerial
////////////////////////////////////////

struct InvalidSerial : public CriticalError {

	enum Error : uint8_t { ConversionFailed, NoDigits };

	explicit InvalidSerial(enum Error e)
	    : CriticalError(std::string("InvalidSerial") + error_str(e))
	{
	}

private:
	static const char *error_str(enum Error e)
	{
		switch (e) {
		case Error::ConversionFailed:
			return "serial conversion failed";
		case Error::NoDigits:
			return "no digits";
		};
		assert(0 && "should never get here");
		return nullptr;
	}
};

////////////////////////////////////////
//      struct OutOfRange
////////////////////////////////////////

struct OutOfRange : public InputError {

	enum Type : uint8_t { Current, Voltage, Wattage };

	OutOfRange(Type t, const double &to_set);

private:
	static const char *type_str(const Type &t);
	static const char *unit_str(const Type &t);
	static std::string to_string(const double &number, const Type &t);
};

OutOfRange::OutOfRange(Type t, const double &to_set)
    : InputError(std::string("OutOfRange: ") + type_str(t) + " " +
		 OutOfRange::to_string(to_set, t))
{
}

const char *OutOfRange::type_str(const Type &t)
{
	switch (t) {
	case Type::Current:
		return "current";
	case Type::Voltage:
		return "voltage";
	case Type::Wattage:
		return "power";
	};
	assert(0 && "should never get here");
	return nullptr;
}

const char *OutOfRange::unit_str(const Type &t)
{
	switch (t) {
	case Type::Current:
		return "uA";
	case Type::Voltage:
		return "kV";
	case Type::Wattage:
		return "mW";
	};
	assert(0 && "should never get here");
	return nullptr;
}

std::string OutOfRange::to_string(const double &number, const Type &t)
{
	char buf[16];
	snprintf(buf, sizeof(buf), "%.01lf%s", number, unit_str(t));
	return std::string(buf);
}

////////////////////////////////////////
//      struct StateError
////////////////////////////////////////

struct StateError : public InputError {

	enum Type : uint8_t { InterlockDisabled, MonitorNotReady };

	explicit StateError(Type t);

private:
	static const char *type_str(const Type &t);
};

StateError::StateError(Type t)
    : InputError(std::string("State: ") + type_str(t))
{
}

const char *StateError::type_str(const Type &t)
{
	switch (t) {
	case Type::InterlockDisabled:
		return "interlock is disabled";
	case Type::MonitorNotReady:
		return "monitor not ready";
	};
	assert(0 && "should never get here");
	return nullptr;
}

////////////////////////////////////////
//      static non member functions
////////////////////////////////////////
static MiniXConfig choose_config(const MiniXDevice &dev)
{
	if (dev.is50KVDevice()) {
		logger::write(logger::V_INFO,
			      "tube identified as 50kV version");
		return MiniXConfig::make50KV();
	}
	logger::write(logger::V_INFO, "tube identified as 40kV version");
	return MiniXConfig::makeDefault();
}

static uint8_t set(const uint8_t &lhs, const uint8_t &mask)
{
	return lhs | mask;
}

static uint8_t clear(const uint8_t &lhs, const uint8_t &mask)
{
	return lhs & ~(mask);
}

static uint64_t parse_serial(const char *bytes)
{
	assert(bytes && "bytes is NULL");

	char *endptr = nullptr;
	auto c = strtoull(bytes, &endptr, 10);
	if (c == ULONG_MAX && errno == ERANGE) {
		// range error
		throw InvalidSerial(InvalidSerial::ConversionFailed);
		;
	}
	if (endptr == bytes) {
		// no digits
		throw InvalidSerial(InvalidSerial::NoDigits);
		;
	}
	return c;
}

////////////////////////////////////////
//      class MiniXDevice
////////////////////////////////////////

MiniXDevice::MiniXDevice(MiniXDevice &&rhs)
    : ctx_(std::forward<struct ftdi_context *>(rhs.ctx_)),
      manufacturer_(std::forward<CharArray>(rhs.manufacturer_)),
      description_(std::forward<CharArray>(rhs.description_)),
      serial_(std::forward<CharArray>(rhs.serial_)),
      num_serial_(std::forward<uint64_t>(rhs.num_serial_))
{
	rhs.ctx_ = nullptr;
}

MiniXDevice &MiniXDevice::operator=(MiniXDevice &&rhs)
{
	if (this->ctx_) {
		ftdi_free(this->ctx_);
	}
	this->ctx_ = std::forward<struct ftdi_context *>(rhs.ctx_);

	this->manufacturer_ = std::forward<CharArray>(rhs.manufacturer_);
	this->description_ = std::forward<CharArray>(rhs.description_);
	this->serial_ = std::forward<CharArray>(rhs.serial_);
	this->num_serial_ = std::forward<uint64_t>(rhs.num_serial_);

	return *this;
}

MiniXDevice::MiniXDevice(struct ftdi_context *ctx, const CharArray &m,
			 const CharArray &d, const CharArray &s)
    : ctx_(ctx), manufacturer_(m), description_(d), serial_(s),
      num_serial_(parse_serial(s.data()))
{
	assert(ctx && "ctx is NULL");
}
/**
 *  close the ftdi context
 * */
MiniXDevice::~MiniXDevice()
{
	if (ctx_) {
		ftdi_free(ctx_);
		ctx_ = nullptr;
	}
}

struct ftdi_context *MiniXDevice::context() const noexcept { return ctx_; }

const MiniXDevice::CharArray &MiniXDevice::manufacturer() const noexcept
{
	return manufacturer_;
}

const MiniXDevice::CharArray &MiniXDevice::description() const noexcept
{
	return description_;
}

const MiniXDevice::CharArray &MiniXDevice::serial() const noexcept
{
	return serial_;
}

const uint64_t &MiniXDevice::num_serial() const noexcept { return num_serial_; }

bool MiniXDevice::isNSI() const noexcept { return num_serial_ > 9999; }

bool MiniXDevice::is50KVDevice() const noexcept
{
	return num_serial_ > 1118880;
}

////////////////////////////////////////
//      struct MiniXConfig
////////////////////////////////////////

MiniXConfig MiniXConfig::make50KV()
{
	MiniXConfig x;
	x.hv_max = 50l;
	x.hv_conversion_factor = 12.5l;
	return x;
}

MiniXConfig MiniXConfig::makeDefault() { return MiniXConfig(); }

MiniXConfig::MiniXConfig()
    : hv_default(15), hv_min(10l), hv_max(40l), hv_conversion_factor(10l),
      curr_default(15.0l), curr_min(5l), curr_max(200l),
      curr_conversion_factor(50.0), vref(4.096l), dac_adc_scale(4096l),
      wattage_max(4l), wattage_safety_margin(0.05l),
      wattage_safety_mw((wattage_max - wattage_safety_margin) * 1000l)
{
}

////////////////////////////////////////
//      class MiniX::Impl
////////////////////////////////////////

class MiniX::Impl
{
	MiniXDevice device_;
	bool device_cleanup_done_;

	const MiniXConfig config_;

	uint8_t mpsse_high_byte_buf_; ///< the ftdi device config
				      ///< hbyte, @see
				      ///< HIGH_BYTE_CONFIG
	uint8_t mpsse_low_byte_buf_;  ///< the ftdi device config
				      ///< lbyte @see LOW_BYTE_CONFIG

	double hv_previous_;      ///< previous voltage for calculating
				  ///< the wattage
	double current_previous_; ///< @see hv_previous_

public:
	static MiniXDevices getDevices();

	explicit Impl(MiniXDevice &&dev);
	Impl(MiniXDevice &&dev, const MiniXConfig &conf);
	~Impl();

	void cleanup();

	Impl(const Impl &rhs) = delete;
	Impl &operator=(const Impl &rhs) = delete;

	const MiniXDevice &getDevice() const noexcept;
	const MiniXConfig &getConfig() const noexcept;
	const uint64_t &serial() const;

	TubeState state();

	static bool monitor_is_ready(const TubeState &s);
	static bool interlock_is_enabled(const TubeState &s);
	static bool hv_is_enabled(const TubeState &s);

	// hv functions
	void enableHV();
	void disableHV();
	bool hvIsEnabled();

	void setHV(const double &val_in_kV);
	double hv();

	// current functions
	void setCurrent(const double &val_in_muA);
	double current();

	// wattage functions in mW
	double wattage() const noexcept;

	// temperature functions
	double temperature();

private:
	/**
	 * Output masks for output and input mode (since the
	 * newest protocoll holds the same value for OUTPUTMODE
	 * and INPUTMODE
	 * */
	enum OUTPUT_MASK : uint8_t {
		OM_HIGH = 0x08, ///< bitmask of the high config
				///< byte, enabled bits hint to
				///< outputs
		OM_LOW = 0x7b   ///< @see OM_HIGH
	};

	/**
	 * project specific register addresses in the low config
	 * byte
	 * */
	enum LOW_BYTE_CONFIG : uint8_t {
		LBC_CLOCK_STATE = 0x1,
		LBC_DATA_STATE = 0x2,
		LBC_ADC_CHIP_SELECT = 0x08, ///< clear the bit to select the adc
		LBC_DAC_CHIP_SELECT = 0x10, ///< clear the bit to select the dac
		LBC_HV_ENABLE_A = 0x20,     ///< enable this to use the tube
		LBC_HV_ENABLE_B = 0x40,     ///< enable this to use the tube
		LBC_MINIX_IS_READY = 0x80   ///< check if monitor is ready
	};

	/**
	 * project specific register addresses in the high config
	 * byte
	 * */
	enum HIGH_BYTE_CONFIG : uint8_t {
		HBC_READ_INTERLOCK_STATE = 0x01, ///< check if interlock is set
		HBC_TS_CHIP_SELECT = 0x08 ///< temperature sensor chip-select
	};

	/**
	 * @brief hex values for selecting the right dac
	 * */
	enum DAC_SELECTION : uint8_t {
		DS_HIGH_VOLTAGE = 0x18, ///< chooses the hv dac
		DS_CURRENT = 0x19       ///< chooses the current dac
	};

	/**
	 * @brief hex values for selecting the right adc
	 * */
	enum ADC_SELECTION : uint8_t {
		AS_HIGH_VOLTAGE = 0xd0, ///< chooses the hv adc
		AS_CURRENT = 0xf0       ///< chooses the current adc
	};

	/**
	 * @brief continuos convert.,
	 *      no shutdown, 12bit resolution (1.2s conversion time)
	 *
	 * The sensor is called DS1722 (from source)
	 * The most precise value is 12bit resolution with 0xe8
	 *
	 * @see DS1722 documentation
	 * */
	struct TempSensor {
		TempSensor() = delete;

		static uint8_t default_config() { return 0xe8; }

		static uint16_t value_range() { return 1 << 12; }
		static double celsius_per_step() { return 0.0625; }

		enum WRITEABLE : uint8_t { W_CONFIG = 0x80 };

		enum READABLE : uint8_t {
			R_STATUS = 0x0,
			R_LSB = 0x1,
			R_MSB = 0x2
		};
	};

	void handleMpsse(bool do_enable);
	void enableMpsse(void);
	void disableMpsse(void);
	void initTemperatureSensor(void);

	void setDacVoltage(enum DAC_SELECTION dac, const double &new_value);

	double readADC(enum ADC_SELECTION as);

	void readConfigBytes(uint8_t *low, uint8_t *high);

	static size_t prepare_clock_divisor(uint8_t *bytes, size_t bytes_n);

	static size_t handle_temperature_sensor(uint8_t *low_byte_tmp,
						uint8_t *high_byte_tmp,
						uint8_t *bytes, size_t bytes_n);
	static double to_temperature(const uint8_t &lsb, const uint8_t &msb);
};

////////////////////////////////////////
//      MiniX::Impl declaration
////////////////////////////////////////

MiniX::MiniXDevices MiniX::Impl::getDevices()
{
	// allocate and init
	struct ftdi_context *ctx = ftdi_new();
	assert(ctx && "ftdi context allocation failed");

	struct ftdi_device_list *list(nullptr);
	int c = ftdi_usb_find_all(ctx, &list, 0x0403, 0xd058);
	if (c < 0) {
		FtdiError err(FtdiError::UsbFindAll, ctx);

		ftdi_free(ctx);
		if (list) {
			ftdi_list_free2(list);
			list = nullptr;
		}
		throw err;
	}

	MiniX::MiniXDevices devices;

	auto it = list;
	while (it) {
		// new ftdi device
		auto tmp = ftdi_new();
		assert(tmp && "ftdi context allocation failed");

		// get some meta data
		MiniXDevice::CharArray manufacturer, description, serial;
		c = ftdi_usb_get_strings(tmp, it->dev, manufacturer.data(),
					 manufacturer.size(),
					 description.data(), description.size(),
					 serial.data(), serial.size());
		if (c) {
			FtdiError err(FtdiError::UsbGetStrings, tmp);
			ftdi_list_free2(list);
			ftdi_free(tmp);
			throw err;
		}

		// create one context for each device
		c = ftdi_usb_open_dev(tmp, it->dev);
		if (c) {
			FtdiError err(FtdiError::UsbGetStrings, tmp);
			ftdi_list_free2(list);
			ftdi_free(tmp);

			throw err;
		}
		try {
			devices.emplace_back(tmp, manufacturer, description,
					     serial);
		} catch (const std::exception &ex) {
			throw CriticalError(ex.what());
		}
		it = it->next;
	}

	ftdi_free(ctx);
	if (list) {
		ftdi_list_free2(list);
	}
	return devices;
}

MiniX::Impl::Impl(MiniXDevice &&dev)
    : Impl(std::forward<MiniXDevice>(dev), choose_config(dev))
{
}

MiniX::Impl::Impl(MiniXDevice &&dev, const MiniXConfig &conf)
    : device_(std::forward<MiniXDevice>(dev)), device_cleanup_done_(false),
      config_(conf), mpsse_high_byte_buf_(0x0), mpsse_low_byte_buf_(0x0),
      hv_previous_(0), current_previous_(0)
{
	enableMpsse();
	initTemperatureSensor();

	uint8_t buffer[2];
	readConfigBytes(buffer, buffer + 1);

	setHV(config_.hv_default);
	setCurrent(config_.curr_default);

	logger::write(logger::V_DEBUG,
		      "MiniX: initialised, (lbyte:0x%x, hbyte:0x%x)", buffer[0],
		      buffer[1]);
}

/**
 * @brief cleanup of the connection
 *
 * This is not mandatory and can throw.
 * The device_cleanup_done_ flag is implemented
 * to prevent multiple calls of this
 * function.
 * In case of a failed cleanup, multiple calling
 * makes no sense either and therefore it is set true
 * before doing the dangerous stuff
 */
void MiniX::Impl::cleanup()
{
	if (device_cleanup_done_) {
		return;
	}
	device_cleanup_done_ = true;

	disableHV();
	disableMpsse();

	logger::write(logger::V_INFO, "MiniX: cleanup() finished");
}

/**
 * @brief just check if the cleanup is done
 */
MiniX::Impl::~Impl()
{
	assert(device_cleanup_done_ &&
	       "call MiniX::cleanup() before destruction");
}

const MiniXDevice &MiniX::Impl::getDevice() const noexcept { return device_; }

const MiniXConfig &MiniX::Impl::getConfig() const noexcept { return config_; }

const uint64_t &MiniX::Impl::serial() const { return device_.num_serial(); }

bool MiniX::Impl::monitor_is_ready(const TubeState &s)
{
	return !(s.low & MiniX::Impl::LOW_BYTE_CONFIG::LBC_MINIX_IS_READY);
}

bool MiniX::Impl::interlock_is_enabled(const TubeState &s)
{
	return s.high & MiniX::Impl::HIGH_BYTE_CONFIG::HBC_READ_INTERLOCK_STATE;
}

bool MiniX::Impl::hv_is_enabled(const TubeState &s)
{
	using lbc = MiniX::Impl::LOW_BYTE_CONFIG;
	return s.low & (lbc::LBC_HV_ENABLE_A | lbc::LBC_HV_ENABLE_B);
}

void MiniX::Impl::enableHV()
{
	TubeState s = state();
	if (!interlock_is_enabled(s)) {
		throw StateError(StateError::InterlockDisabled);
	}
	if (!monitor_is_ready(s)) {
		throw StateError(StateError::MonitorNotReady);
	}

	using lbc = LOW_BYTE_CONFIG;
	uint8_t bytes[8];
	size_t pos = 0;

	uint8_t low_byte_tmp = set(mpsse_low_byte_buf_,
				   lbc::LBC_HV_ENABLE_A | lbc::LBC_HV_ENABLE_B);

	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int c = ftdi_write_data(ctx, bytes, pos);
	if (c < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert(c == (int)pos && "make while loop here");

	mpsse_low_byte_buf_ = low_byte_tmp;
	logger::write(logger::V_INFO, "enableHV(): executed");
}

void MiniX::Impl::disableHV()
{
	using lbc = LOW_BYTE_CONFIG;
	uint8_t bytes[8];
	size_t pos = 0;

	uint8_t low_byte_tmp = clear(
	    mpsse_low_byte_buf_, lbc::LBC_HV_ENABLE_A | lbc::LBC_HV_ENABLE_B);

	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int c = ftdi_write_data(ctx, bytes, pos);
	if (c < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert(c == (int)pos && "make while loop here");

	mpsse_low_byte_buf_ = low_byte_tmp;
	logger::write(logger::V_INFO, "disableHV(): executed");
}

bool MiniX::Impl::hvIsEnabled()
{
	uint8_t buf = 0;
	readConfigBytes(&buf, nullptr);

	using lbc = LOW_BYTE_CONFIG;
	return (buf & (lbc::LBC_HV_ENABLE_A | lbc::LBC_HV_ENABLE_B));
}

MiniX::TubeState MiniX::Impl::state()
{
	uint8_t low = 0, high = 0;
	readConfigBytes(&low, &high);
	return { low, high };
}

void MiniX::Impl::setHV(const double &val_in_kV)
{
	double power = current_previous_ * val_in_kV;
	if (power > config_.wattage_safety_mw) {
		throw OutOfRange(OutOfRange::Wattage, power);
	}

	setDacVoltage(DAC_SELECTION::DS_HIGH_VOLTAGE, val_in_kV);
	hv_previous_ = val_in_kV;

	logger::write(logger::V_INFO, "setHV(): set to %.02lfkV", val_in_kV);

	logger::write(
	    logger::V_DEBUG, "setHV: wattage now: %.02lfmW (max:%.02lfmW)",
	    current_previous_ * hv_previous_, config_.wattage_safety_mw);
}

double MiniX::Impl::hv() { return readADC(ADC_SELECTION::AS_HIGH_VOLTAGE); }

void MiniX::Impl::setCurrent(const double &val_in_muA)
{
	double power = val_in_muA * hv_previous_;
	if (power > config_.wattage_safety_mw) {
		throw OutOfRange(OutOfRange::Wattage, power);
	}

	setDacVoltage(DAC_SELECTION::DS_CURRENT, val_in_muA);
	current_previous_ = val_in_muA;

	logger::write(logger::V_INFO, "setCurrent(): set to %.02lfuA",
		      val_in_muA);

	logger::write(
	    logger::V_DEBUG, "setCurrent: wattage now: %.02lfmW (max:%.02lfmW)",
	    current_previous_ * hv_previous_, config_.wattage_safety_mw);
}

double MiniX::Impl::current() { return readADC(ADC_SELECTION::AS_CURRENT); }

double MiniX::Impl::wattage() const noexcept
{
	return hv_previous_ * current_previous_;
}

double MiniX::Impl::temperature()
{
	// clear data and clock state, data stays low all the time
	using lbc = LOW_BYTE_CONFIG;
	using hbc = HIGH_BYTE_CONFIG;
	uint8_t low_byte_tmp = clear(
	    mpsse_low_byte_buf_, lbc::LBC_CLOCK_STATE | lbc::LBC_DATA_STATE);
	uint8_t high_byte_tmp =
	    set(mpsse_high_byte_buf_, hbc::HBC_TS_CHIP_SELECT);

	size_t pos = 0;
	uint8_t bytes[64];

	pos += prepare_clock_divisor(bytes, sizeof(bytes));

	pos += handle_temperature_sensor(&low_byte_tmp, &high_byte_tmp,
					 bytes + pos, sizeof(bytes) - pos);

	// write bits, not bytes
	bytes[pos++] = MPSSE_DO_WRITE | MPSSE_BITMODE;
	bytes[pos++] = 0x07; // number of bits: 8
	bytes[pos++] = 0x01;

	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	// now read
	bytes[pos++] = MPSSE_DO_READ;
	bytes[pos++] = 0x01; // 2 bytes
	bytes[pos++] = 0x00;

	high_byte_tmp = clear(high_byte_tmp, hbc::HBC_TS_CHIP_SELECT);
	bytes[pos++] = SET_BITS_HIGH;
	bytes[pos++] = high_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_HIGH;
	assert(sizeof(bytes) > pos && "enlarge the byte buffer!");

	// now write
	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int c = ftdi_write_data(ctx, bytes, pos);
	if (c < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert(c == (int)pos && "make while loop here");

	uint8_t rxbuf[2];
	size_t recvd = 0;
	c = 0;
	while (c >= 0 && recvd < 2) {
		c = ftdi_read_data(ctx, rxbuf + recvd, sizeof(rxbuf) - recvd);
		recvd += c;
	}
	if (c < 0) {
		throw FtdiError(FtdiError::ReadData, ctx);
	}
	logger::write(logger::V_DEBUG,
		      "temperature: returned %d bytes (msb:%d, lsb:%d)\n",
		      recvd, rxbuf[1], rxbuf[0]);
	assert(c == 2 && "less than 2 bytes returned (?)");

	// ok, buffered data to member
	mpsse_high_byte_buf_ = high_byte_tmp;
	mpsse_low_byte_buf_ = low_byte_tmp;

	return to_temperature(rxbuf[0], rxbuf[1]);
}

void MiniX::Impl::handleMpsse(bool do_enable)
{

	struct ftdi_context *const ctx = device_.context();
	assert(ctx && "ctx is NULL");

	logger::write(logger::V_DEBUG, "handle_mpsse(do_enable = %s): called\n",
		      (do_enable) ? "true" : "false");

	if (do_enable) {
		int r = ftdi_set_latency_timer(ctx, 0x4);
		if (r) {
			throw FtdiError(FtdiError::SetLatencyTimer, ctx);
		}
		// rewrite timeouts
		ctx->usb_read_timeout = 100;
		ctx->usb_write_timeout = 40;
	}

	// set bitmode (mpsse on)
	int r = ftdi_set_bitmode(ctx, 0,
				 (do_enable) ? BITMODE_MPSSE : BITMODE_RESET);
	if (r) {
		throw FtdiError(FtdiError::SetBitmode, ctx);
	}

	// cmd disable returns here
	if (!do_enable) {
		return;
	}

	using lbc = LOW_BYTE_CONFIG;
	using hbc = HIGH_BYTE_CONFIG;

	// init these for the first time
	mpsse_high_byte_buf_ =
	    clear(OUTPUT_MASK::OM_HIGH, hbc::HBC_TS_CHIP_SELECT);
	mpsse_low_byte_buf_ = clear(
	    OUTPUT_MASK::OM_LOW, lbc::LBC_HV_ENABLE_A | lbc::LBC_HV_ENABLE_B);

	uint8_t bytes[32];
	size_t pos = 0;

	bytes[pos++] = SET_BITS_HIGH;
	bytes[pos++] = mpsse_high_byte_buf_;
	bytes[pos++] = OUTPUT_MASK::OM_HIGH;

	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = mpsse_low_byte_buf_;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	int n = ftdi_write_data(ctx, bytes, pos);
	if (n < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert(n == (int)pos && "ok, do the copy-until...");
}

void MiniX::Impl::enableMpsse() { handleMpsse(true); }

void MiniX::Impl::disableMpsse() { handleMpsse(false); }

void MiniX::Impl::initTemperatureSensor(void)
{
	using lbc = LOW_BYTE_CONFIG;
	using hbc = HIGH_BYTE_CONFIG;

	size_t pos = 0;
	uint8_t bytes[64];

	uint8_t low_byte_tmp = clear(mpsse_low_byte_buf_, lbc::LBC_CLOCK_STATE);
	uint8_t high_byte_tmp =
	    set(mpsse_high_byte_buf_, hbc::HBC_TS_CHIP_SELECT);

	pos += prepare_clock_divisor(bytes, sizeof(bytes));

	pos += handle_temperature_sensor(&low_byte_tmp, &high_byte_tmp,
					 bytes + pos, sizeof(bytes) - pos);

	bytes[pos++] = INVERT_DTR;
	bytes[pos++] = 0x01; // 0x01 equals 2byte output
	bytes[pos++] = 0x00; // high byte lenght = 0
	bytes[pos++] = TempSensor::WRITEABLE::W_CONFIG;
	bytes[pos++] = TempSensor::default_config();

	bytes[pos++] = SET_BITS_HIGH;
	high_byte_tmp = clear(high_byte_tmp, hbc::HBC_TS_CHIP_SELECT);
	bytes[pos++] = high_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_HIGH;
	assert(sizeof(bytes) > pos && "enlarge the byte buffer!");

	// now write
	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int c = ftdi_write_data(ctx, bytes, pos);
	if (c < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert((size_t)c == pos && "make while loop here");

	// ok, buffered data to member
	mpsse_high_byte_buf_ = high_byte_tmp;
	mpsse_low_byte_buf_ = low_byte_tmp;
}

/**
 * @page dac_example Calculate a dac value
 *
 * example calculating the integer value of a given double:<br>
 * high voltage = 15kV<br>
 * 15 / 4.096 * 4096 = 15 * 1000 = 15000<br>
 * <br>
 * hex(15000) = 0x3a98<br>
 * to low: (0x3a98 & 0x0f) << 4 = (0x8) << 4 = 0x80<br>
 * to high (0x3a98) & 0x0ff0) >> 4 = (0xa90) >> 4 = 0xa9<br>
 * */

/**
 * @brief set dac voltage for voltage and current
 *
 * @param dac either current or voltage dac
 *
 * @param val value in the appropiate format (kV and muA)
 */
void MiniX::Impl::setDacVoltage(enum DAC_SELECTION dac, const double &val)
{
	using lbc = LOW_BYTE_CONFIG;

	uint16_t int_val = 0;
	switch (dac) {
	case DAC_SELECTION::DS_HIGH_VOLTAGE:
		if (val > config_.hv_max || val < config_.hv_min) {
			throw OutOfRange(OutOfRange::Voltage, val);
		}
		int_val = val / config_.hv_conversion_factor / config_.vref *
			  config_.dac_adc_scale;
		break;
	case DAC_SELECTION::DS_CURRENT:
		if (val > config_.curr_max || val < config_.curr_min) {
			throw OutOfRange(OutOfRange::Current, val);
		}
		int_val = val / config_.curr_conversion_factor / config_.vref *
			  config_.dac_adc_scale;
		break;
	default:
		assert(0 && "this must never happen");
	};

	// high      low
	// 0111 1111 1111 0000
	const uint8_t converted_low_byte = (int_val & 0x0f) << 4;
	const uint8_t converted_high_byte = (int_val & 0x0ff0) >> 4;

	// make tmp value, disable the DAC and pull up the clock
	// @note some devices have an inverted chip-select
	//      and since the original software is working,
	//      this seems to be the case here
	uint8_t low_byte_tmp =
	    clear(mpsse_low_byte_buf_, lbc::LBC_DAC_CHIP_SELECT);
	low_byte_tmp = set(low_byte_tmp, lbc::LBC_CLOCK_STATE);
	logger::write(
	    logger::V_DEBUG, "setDACVoltage: dac:%s lbyte:0x%x hbyte:0x%x",
	    (dac == DAC_SELECTION::DS_CURRENT) ? "current" : "voltage",
	    converted_low_byte, converted_high_byte);

	size_t pos = 0;
	uint8_t bytes[32];
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	bytes[pos++] = INVERT_DTR;
	bytes[pos++] = 0x02; // low byte lenght: 3 bytes to send
	bytes[pos++] = 0x0;  // high byte lenght: 0

	// the dac value: address + 2byte value
	bytes[pos++] = dac; // register address
	bytes[pos++] = converted_high_byte;
	bytes[pos++] = converted_low_byte;

	// unset the dac chip
	low_byte_tmp = set(low_byte_tmp, lbc::LBC_DAC_CHIP_SELECT);
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;
	assert(pos < sizeof(bytes) && "whoops, enlarge the buffer");

	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int r = ftdi_write_data(ctx, bytes, pos);
	if (r < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert(r == (int)pos && "not completely written...");

	// everything worked, tmp -> member
	mpsse_low_byte_buf_ = low_byte_tmp;
}

/**
 * @brief read the specified ADC
 *
 * @param as the selected ADC
 *
 * @return value in kV or muA
 */
double MiniX::Impl::readADC(enum ADC_SELECTION as)
{
	using lbc = LOW_BYTE_CONFIG;

	uint8_t bytes[64];
	size_t pos = 0;

	// clk divisor
	pos += prepare_clock_divisor(bytes, sizeof(bytes));

	uint8_t low_byte_tmp =
	    clear(mpsse_low_byte_buf_,
		  lbc::LBC_CLOCK_STATE | lbc::LBC_ADC_CHIP_SELECT);

	// mpsse line, ADCCS low
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	// control nibble
	bytes[pos++] = MPSSE_DO_WRITE | MPSSE_BITMODE | MPSSE_WRITE_NEG;
	bytes[pos++] = 0x03; // 4 bits
	bytes[pos++] = as;

	// TODO check if this is needed: already is set to input
	// data direction to input
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	// read 2 bytes
	bytes[pos++] = MPSSE_DO_READ;
	bytes[pos++] = 0x01;
	bytes[pos++] = 0x00;

	// set adc chip select, now write and wait for rx
	low_byte_tmp = set(low_byte_tmp, lbc::LBC_ADC_CHIP_SELECT);
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;
	assert(pos < sizeof(bytes) && "whoops, enlarge buffer");

	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int c = ftdi_write_data(ctx, bytes, pos);
	if (c < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert(c == (int)pos && "make while loop here");

	// worked, tmp to member
	mpsse_low_byte_buf_ = low_byte_tmp;

	// now read
	uint8_t rxbuf[2];
	size_t recvd = 0;
	c = 0;
	while (c >= 0 && recvd < 2) {
		c = ftdi_read_data(ctx, rxbuf + recvd, sizeof(rxbuf) - recvd);
		recvd += c;
	}
	if (c < 0) {
		throw FtdiError(FtdiError::ReadData, ctx);
	}
	logger::write(logger::V_DEBUG,
		      "readADC: returned %d bytes (msb:%d, lsb:%d)\n", recvd,
		      rxbuf[1], rxbuf[0]);
	assert(c == 2 && "less than 2 bytes returned (?)");

	// more or less copied from the manufacturers MiniXDlg file
	const double voltage = (((rxbuf[0] >> 3) << 8) |
				((rxbuf[0] & 0x07) << 5 | (rxbuf[1] >> 3))) *
			       config_.vref / config_.dac_adc_scale;

	return (as == ADC_SELECTION::AS_HIGH_VOLTAGE)
		   ? voltage * config_.hv_conversion_factor
		   : voltage * config_.curr_conversion_factor;
}

/**
 * @brief read low and high config byte
 *
 * @param low NULL or ptr to uint8_t
 *
 * @param high NULL or ptr to uint8_t
 *
 * Since the config bytes hint to
 * open/closed interlocks and other stuff,
 * read them.
 * */
void MiniX::Impl::readConfigBytes(uint8_t *low, uint8_t *high)
{
	assert((low || high) && "both bytes are NULL");
	uint8_t bytes[2];
	size_t pos = 0;

	bytes[pos++] = GET_BITS_LOW;
	bytes[pos++] = GET_BITS_HIGH;

	auto ctx = device_.context();
	assert(ctx && "ctx is NULL");
	int c = ftdi_write_data(ctx, bytes, pos);
	if (c < 0) {
		throw FtdiError(FtdiError::WriteData, ctx);
	}
	assert((size_t)c == pos && "make while loop here");

	uint8_t rxbuf[2];
	size_t recvd = 0;
	c = 0;
	while (c >= 0 && recvd < sizeof(rxbuf)) {
		c = ftdi_read_data(ctx, rxbuf + recvd, sizeof(rxbuf) - recvd);
		recvd += c;
	}
	if (c < 0) {
		throw FtdiError(FtdiError::ReadData, ctx);
	}

	if (low) {
		*low = rxbuf[0];
	}
	if (high) {
		*high = rxbuf[1];
	}
}

size_t MiniX::Impl::prepare_clock_divisor(uint8_t *bytes, size_t bytes_n)
{
	assert(bytes && "bytes is NULL");
	assert(bytes_n >= 3 && "not enough space left");

	bytes[0] = TCK_DIVISOR;
	bytes[1] = 0x03;
	bytes[2] = 0x00;
	return 3;
}

size_t MiniX::Impl::handle_temperature_sensor(uint8_t *low_byte_tmp,
					      uint8_t *high_byte_tmp,
					      uint8_t *bytes, size_t bytes_n)
{
	assert(low_byte_tmp && "is NULL");
	assert(high_byte_tmp && "is NULL");
	assert(bytes && "byte array is NULL");

	size_t pos = 0;
	// pull clock low
	// @note this should announce a transmission
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = *low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	// set termperature sensor chip select high
	bytes[pos++] = SET_BITS_HIGH;
	bytes[pos++] = *high_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_HIGH;

	// clock out 2 bytes
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = *low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = *low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	// push clockstate up again
	// @note this frees the bus so another device can send
	*low_byte_tmp = set(*low_byte_tmp, LOW_BYTE_CONFIG::LBC_CLOCK_STATE);
	bytes[pos++] = SET_BITS_LOW;
	bytes[pos++] = *low_byte_tmp;
	bytes[pos++] = OUTPUT_MASK::OM_LOW;

	assert(bytes_n > pos && "enlarge the byte buffer");
	return pos;
}

double MiniX::Impl::to_temperature(const uint8_t &lsb, const uint8_t &msb)
{
	const uint16_t i = (msb << 4) | (lsb >> 4);
	return ((msb & 1 << 8) ? (i - TempSensor::value_range()) : i) *
	       TempSensor::celsius_per_step();
}

/*
size_t MiniX::Impl::push_low_byte(uint8_t *buffer, uint8_t value)
{
	buffer[0] = SET_BITS_LOW;
	buffer[1] = value;
	buffer[2] = OUTPUT_MASK::OM_LOW;
	return 3;
}

size_t MiniX::Impl::push_high_byte(uint8_t *buffer, uint8_t value)
{
	buffer[0] = SET_BITS_HIGH;
	buffer[1] = value;
	buffer[2] = OUTPUT_MASK::OM_HIGH;
	return 3;
}
*/

////////////////////////////////////////
//      class MiniX
////////////////////////////////////////

MiniX::MiniXDevices MiniX::getDevices() { return Impl::getDevices(); }

MiniX::MiniX(MiniXDevice &&device)
    : impl_(new Impl(std::forward<MiniXDevice>(device)))
{
}

MiniX::MiniX(MiniXDevice &&device, const MiniXConfig &conf)
    : impl_(new Impl(std::forward<MiniXDevice>(device), conf))
{
}

void MiniX::cleanup()
{
	assert(impl_ && "impl is NULL!");
	impl_->cleanup();
}

MiniX::~MiniX() {}

MiniX::MiniX(MiniX &&rhs) : impl_(std::forward<MiniX>(rhs).impl_) {}

MiniX &MiniX::operator=(MiniX &&rhs)
{
	assert(&rhs != this && "move operation into itself");
	this->impl_ = std::forward<MiniX>(rhs).impl_;
	return *this;
}

/**
 * @brief the previously moved device
 *
 * @return ref to the MiniXDevice
 */
const MiniXDevice &MiniX::getDevice() const noexcept
{
	assert(impl_ && "impl is NULL!");
	return impl_->getDevice();
}

/**
 * @brief get the copied config
 *
 * @return ref to the copied config
 */
const MiniXConfig &MiniX::getConfig() const noexcept
{
	assert(impl_ && "impl is NULL!");
	return impl_->getConfig();
}

/**
 * @brief serial as a number
 */
const uint64_t &MiniX::serial() const noexcept
{
	assert(impl_ && "impl is NULL!");
	return impl_->serial();
}

/**
 * @brief enable the tube
 */
void MiniX::enableHV()
{
	assert(impl_ && "impl is NULL!");
	impl_->enableHV();
}

/**
 * @brief disable the tube
 */
void MiniX::disableHV()
{
	assert(impl_ && "impl is NULL!");
	impl_->disableHV();
}

/**
 * @brief Checks the usb device enable flags
 *
 * @return true if enabled
 */
bool MiniX::hvIsEnabled()
{
	assert(impl_ && "impl is NULL!");
	return impl_->hvIsEnabled();
}

/**
 * @brief check all state flags
 *
 * @return MiniX::TubeState
 */
MiniX::TubeState MiniX::state()
{
	assert(impl_ && "impl is NULL!");
	return impl_->state();
}

/**
 * @brief check if monitor signals ready
 *
 * @param s TubeState, the low and high config byte
 *
 * @return true if ready
 */
bool MiniX::monitor_is_ready(const TubeState &s)
{
	return Impl::monitor_is_ready(s);
}

/**
 * @brief check if interlock is enabled
 *
 * @param s TubeState, the low and high config byte
 *
 * @return true if enabled
 */
bool MiniX::interlock_is_enabled(const TubeState &s)
{
	return Impl::interlock_is_enabled(s);
}

/**
 * @brief check if hv is enabled
 *
 * @param s TubeState, the low and high config byte
 *
 * @return true if enabled
 */
bool MiniX::hv_is_enabled(const TubeState &s) { return Impl::hv_is_enabled(s); }

/**
 * @brief set the voltage in kV
 *
 * @param val_in_kV
 */
void MiniX::setHV(const KiloVolt &val_in_kV)
{
	assert(impl_ && "impl is NULL!");
	impl_->setHV(val_in_kV);
}

/**
 * @brief query usb device for voltage in kV
 *
 * @return voltage in kV
 */
MiniX::KiloVolt MiniX::hv()
{
	assert(impl_ && "impl is NULL!");
	return impl_->hv();
}

/**
 * @brief set current in MicroAmpere
 *
 * @param val_in_muA
 */
void MiniX::setCurrent(const MicroAmpere &val_in_muA)
{
	assert(impl_ && "impl is NULL!");
	impl_->setCurrent(val_in_muA);
}

/**
 * @brief query usb device for current
 *
 * @return current in microampere
 */
MiniX::MicroAmpere MiniX::current()
{
	assert(impl_ && "impl is NULL!");
	return impl_->current();
}

/**
 * @brief the configured wattage
 *
 * This is calculated by taking
 * the last configured current and voltage
 *
 * @return wattage in milliWatt
 */
MiniX::MilliWatt MiniX::wattage() const noexcept
{
	assert(impl_ && "impl is NULL!");
	return impl_->wattage();
}

/**
 * @brief query usb device for temperature
 *
 * @return temperature in celsius
 */
MiniX::Celsius MiniX::temperature()
{
	assert(impl_ && "impl is NULL!");
	return impl_->temperature();
}

} // namespace amptek
