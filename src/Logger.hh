

/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef LOGGER_HH_INCLUDED
#define LOGGER_HH_INCLUDED

#include <cstdint>

namespace logger {

enum VERBOSITY : uint8_t {
    V_EMERG,
    V_ALERT,
    V_CRIT,
    V_ERR,
    V_WARNING,
    V_NOTICE,
    V_INFO,
    V_DEBUG
};

void set_verbosity(enum VERBOSITY v);
const enum VERBOSITY &verbosity();

void write(const enum VERBOSITY &v, const char *fmt, ...);

} // namespace logger

#endif // LOGGER_HH_INCLUDED
