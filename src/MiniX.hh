/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef MINIX_HH_INCLUDED
#define MINIX_HH_INCLUDED

#include <array>
#include <memory>
#include <string>
#include <vector>

struct ftdi_context;

namespace amptek
{

/**
 * @brief Base exception class
 */
class MiniXException : public std::exception
{
	std::string msg_;

public:
	explicit MiniXException(const char *err);
	explicit MiniXException(const std::string &err);

	const char *what() const noexcept override;

}; // class MiniXException

/**
 * @brief Given input values are out of range
 *
 * The given values will be ignored
 */
class InputError : public MiniXException
{
public:
	explicit InputError(const char *err);
	explicit InputError(const std::string &err);
}; // class InputError

/**
 * @brief A serious error happened for instance
 *      in the usb connection
 *      or the tube itself
 *
 * If these errors are not handled the tube state
 *      is more or less undefined!
 */
class CriticalError : public MiniXException
{
public:
	explicit CriticalError(const char *err);
	explicit CriticalError(const std::string &err);
}; // class CriticalError

/**
 *  @brief struct MiniXDevice
 *
 *  This holds the usb node
 * */
class MiniXDevice
{
public:
	using CharArray = std::array<char, 64>;

private:
	/**
	 * @brief opaque, see ftdi.h
	 * */
	struct ftdi_context *ctx_;

	// get this info from the usb structure
	CharArray manufacturer_;
	CharArray description_;
	CharArray serial_;
	uint64_t num_serial_;

public:
	MiniXDevice(const MiniXDevice &dev) = delete;
	MiniXDevice operator=(const MiniXDevice &dev) = delete;

	MiniXDevice(MiniXDevice &&dev);
	MiniXDevice &operator=(MiniXDevice &&dev);

	struct ftdi_context *context() const noexcept;
	const CharArray &manufacturer() const noexcept;
	const CharArray &description() const noexcept;
	const CharArray &serial() const noexcept;
	const uint64_t &num_serial() const noexcept;

	bool isNSI() const noexcept;
	bool is50KVDevice() const noexcept;

	explicit MiniXDevice(struct ftdi_context *ctx,
			     const CharArray &manufacturer,
			     const CharArray &description,
			     const CharArray &serial);
	/**
	 *  close usb connection
	 * */
	~MiniXDevice();
}; // class MiniXDevice

/**
 * The default configuration,
 * is loaded by a config file ( at first: mem )
 * */
struct MiniXConfig {

	double hv_default;
	double hv_min;
	double hv_max;
	double hv_conversion_factor;

	double curr_default;
	double curr_min;
	double curr_max;
	double curr_conversion_factor;

	double vref;	  // DAC/DAC reference voltage
	double dac_adc_scale; // 2^resolution bits wide

	double wattage_max;
	double wattage_safety_margin;
	double wattage_safety_mw;

	////////////////////////////////////////////////////////////
	//      static generators
	////////////////////////////////////////////////////////////

	static struct MiniXConfig make50KV();
	static struct MiniXConfig makeDefault();

private:
	explicit MiniXConfig();
};

class MiniX
{
	class Impl;
	std::unique_ptr<class Impl> impl_;

public:
	using MiniXDevices = std::vector<MiniXDevice>;
	static MiniXDevices getDevices();

	explicit MiniX(MiniXDevice &&device); // specify by device
	MiniX(MiniXDevice &&device, const MiniXConfig &conf);

	void cleanup();
	~MiniX();

	MiniX(MiniX &&rhs);
	MiniX &operator=(MiniX &&rhs);

	// TODO: if you write this, verify the serial number of the device!
	// void resetDevice();

	// const construction values
	// or device readouts
	const MiniXDevice &getDevice() const noexcept;
	const MiniXConfig &getConfig() const noexcept;
	const uint64_t &serial() const noexcept;

	struct TubeState {
		uint8_t low;
		uint8_t high;
	};
	TubeState state();

	static bool monitor_is_ready(const TubeState &s);
	static bool interlock_is_enabled(const TubeState &s);
	static bool hv_is_enabled(const TubeState &s);

	// hv functions
	void enableHV();
	void disableHV();
	bool hvIsEnabled();

	using KiloVolt = double;
	void setHV(const KiloVolt &val_in_kV);
	KiloVolt hv();

	// current functions
	using MicroAmpere = double;
	void setCurrent(const MicroAmpere &val_in_muA);
	MicroAmpere current();

	// wattage functions
	using MilliWatt = double;
	MilliWatt wattage() const noexcept;

	// temperature functions
	using Celsius = double;
	Celsius temperature();

}; // class MiniX

} // namespace amptek

#endif // MINIX_HH_INCLUDED
