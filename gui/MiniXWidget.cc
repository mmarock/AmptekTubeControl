

/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "MiniXWidget.hh"

#include <QGridLayout>
#include <QLabel>

#include <cassert>

#include "Control.hh"
#include "MiniXUpdater.hh"
#include "Monitor.hh"
#include "TubeStatusBar.hh"

namespace amptek
{

////////////////////////////////////////////////////////////
//      class MiniXWidget declaration
////////////////////////////////////////////////////////////

MiniXWidget::MiniXWidget(QWidget *parent, MiniXDevice &&dev)
    : QMainWindow(parent)
{
	setupUi(std::forward<MiniXDevice>(dev));
}

MiniXWidget::~MiniXWidget() {}

void MiniXWidget::setupUi(MiniXDevice &&dev)
{
	QWidget *total = new QWidget(this);
	assert(total && "is NULL");

	QGridLayout *grid = new QGridLayout(total);
	assert(grid && "is NULL");

	Monitor *monitor = new Monitor(total);
	assert(monitor && "is NULL");

	MiniXUpdater *updater =
	    new MiniXUpdater(total, std::forward<MiniXDevice>(dev));
	assert(updater && "is NULL");

	connect(updater, &MiniXUpdater::valuesUpdated, monitor,
		&Monitor::updateData);
	connect(updater, &MiniXUpdater::criticalError, this,
		&MiniXWidget::failState);

	Control *control = new Control(total, updater->getMiniX());
	assert(control && "is NULL");

	connect(updater, &MiniXUpdater::valuesUpdated, control,
		&Control::enableHvButton);
	connect(control, &Control::hvOnPressed, updater, &MiniXUpdater::start);
	connect(control, &Control::hvOffPressed, updater, &MiniXUpdater::stop);
	connect(control, &Control::currentChanged, updater,
		&MiniXUpdater::setCurrent);
	connect(control, &Control::voltageChanged, updater,
		&MiniXUpdater::setVoltage);

	QLabel *serial_l = new QLabel(total);
	assert(serial_l && "is NULL");

	serial_l->setAlignment(Qt::AlignCenter);
	serial_l->setText(
	    QString("Amptek Mini-X Device serial: %1")
		.arg(updater->getMiniX().getDevice().num_serial()));

	grid->addWidget(serial_l, 0, 0, 1, 2);
	grid->addWidget(monitor, 1, 0, 1, 1);
	grid->addWidget(control, 1, 1, 1, 1);

	total->setLayout(grid);

	TubeStatusBar *sb = new TubeStatusBar(this);
	assert(sb && "is NULL");

	sb->showMessage("ready", 5000);
	connect(updater, &MiniXUpdater::valuesUpdated, sb,
		&TubeStatusBar::updateStates);
	connect(updater, &MiniXUpdater::inputError, sb,
		&TubeStatusBar::tempMessage);

	this->setStatusBar(sb);
	this->setCentralWidget(total);
}

} // namespace amptek
