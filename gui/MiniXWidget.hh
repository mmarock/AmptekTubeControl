

#ifndef MINIXWIDGET_HH_INCLUDED
#define MINIXWIDGET_HH_INCLUDED

#include <QMainWindow>

namespace amptek
{

class MiniXDevice;

class MiniXWidget : public QMainWindow
{

	Q_OBJECT

public:
	MiniXWidget(QWidget *parent, MiniXDevice &&dev);
	~MiniXWidget();

signals:
	void failState(QString msg);

private:
	void setupUi(MiniXDevice &&dev);
};

} // namespace amptek

#endif // MINIXWIDGET_HH_INCLUDED
