

#ifndef MONITOR_HH_INCLUDED
#define MONITOR_HH_INCLUDED

#include <QWidget>

#include "MiniXUpdater.hh"

namespace amptek
{

class CurrentMonitor;
class PowerMonitor;
class TemperatureMonitor;
class VoltageMonitor;

class Monitor : public QWidget
{
	Q_OBJECT

	VoltageMonitor *voltage_;
	CurrentMonitor *current_;
	PowerMonitor *power_;
	TemperatureMonitor *temperature_;

public:
	explicit Monitor(QWidget *parent);

public slots:
	void updateData(struct MiniXUpdater::Readable r);

signals:
	void dataUpdated();

private:
	void setupUi();
};

} // namespace amptek

#endif // MONITOR_HH_INCLUDED
