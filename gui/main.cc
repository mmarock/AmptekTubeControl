/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include <QAbstractButton>
#include <QApplication>
#include <QMessageBox>

#include "MiniXWidget.hh"

#include "../src/Logger.hh"
#include "../src/MiniX.hh"

#include <cassert>

/**
 * \mainpage AmptekTubeControl documentation
 *
 * \section NAME
 *      AmptekTubeControl
 * \section SYNOPSIS
 *      AmptekTubeControl [SERIAL]<br>
 *      <br>
 *      Control the Mini-X 40kV and 50kV tubes from Amptek
 *      with linux, windows, osx...(basically every OS where libusb and Qt5
 *      runs). The optional SERIAL can be given if there is more than one connected
 *      device. Without the argument the first detected tube is taken.
 *
 * \section INSTALL
 *      You need cmake, libusb, libftdi and the Qt5 framework.
 *      If these are installed and can be found by cmake a regular
 *      workflow can be done:<br>
 *      <br>
 *      cmake path/to/repo<br>
 *      make<br>
 *      <br>
 *      The executable is located in the gui directory.
 * */

int main(int argc, char **argv)
{

	logger::set_verbosity(logger::V_INFO);

	// query devices
	auto devs = amptek::MiniX::getDevices();

	// print all found serials
	logger::write(logger::V_INFO, "-- found devices: %d", devs.size());
	for (const auto &d : devs) {
		logger::write(logger::V_INFO, "\t-> serial:%s",
			      d.serial().data());
	}

	// no device
	if (devs.empty()) {
		return -1;
	}

	// choose device, is a serial given at cmd line?
	auto it = devs.begin();
	if (argc > 1) {
		it = std::find_if(devs.begin(), devs.end(),
				  [&](const amptek::MiniXDevice &d) {
					  auto str1 = argv[1];
					  auto str2 = d.serial().data();
					  auto str1len = strlen(str1);
					  auto str2len = strlen(str2);
					  return (str1len == str2len) &&
						 !strncmp(str1, str2, str2len);
				  });
		if (it == devs.end()) {
			logger::write(logger::V_CRIT,
				      "-- no device with serial %s", argv[1]);
			return -1;
		}
	}
	logger::write(logger::V_INFO, "-- init device with serial %s",
		      it->serial().data());

	QApplication app(argc, argv);
	amptek::MiniXWidget mw(nullptr, std::move(*it));
	mw.setWindowTitle("AmptekTubeControl");

	QMessageBox box;
	box.setWindowTitle("Critical error");
	box.setStandardButtons(QMessageBox::Ok);
	box.setDefaultButton(QMessageBox::Ok);
	box.setIcon(QMessageBox::Critical);

	QWidget::connect(&box, &QMessageBox::buttonClicked, [&]() {
		mw.close();
		box.close();
		app.quit();
	});

	QWidget::connect(&mw, &amptek::MiniXWidget::failState, &box,
			 [&](const QString &str) {
				 box.setText(str);
				 box.exec();
			 });

	mw.show();
	return app.exec();
}
