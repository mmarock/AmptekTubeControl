

/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef MINIXUPDATER_HH_INCLUDED
#define MINIXUPDATER_HH_INCLUDED

#include <QObject>

#include "../src/MiniX.hh"

class QTimer;

namespace amptek
{

class MiniX;
class MiniXDevice;

class MiniXUpdater : public QObject
{
	Q_OBJECT

	QTimer *updater_;
	MiniX device_;
	bool device_failed_;

	bool do_start_;
	bool do_stop_;

	bool set_voltage_;
	double voltage_buf_;

	bool set_current_;
	double current_buf_;

public:
	struct Readable {
		bool device_is_ready;
		bool interlock_is_enabled;
		bool hv_is_enabled;
		double voltage;
		double current;
		double power;
		double temperature;
	};

	MiniXUpdater(QObject *parent, MiniXDevice &&dev);
	~MiniXUpdater();

	const MiniX &getMiniX() const noexcept;

public slots:
	void start() noexcept;
	void stop() noexcept;
	void setCurrent(double curr) noexcept;
	void setVoltage(double voltage) noexcept;

signals:
	void valuesUpdated(struct Readable r);

	void inputError(QString err);
	void criticalError(QString err);

private:
	void setup();
	void onTimeout();
};

} // namespace amptek

#endif // MINIXUPDATER_HH_INCLUDED
