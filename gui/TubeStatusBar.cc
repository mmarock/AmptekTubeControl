

#include "TubeStatusBar.hh"

#include <QFrame>
#include <QLabel>
#include <QVBoxLayout>

#include <cassert>

namespace amptek
{

////////////////////////////////////////////////////////////
//      static non members
////////////////////////////////////////////////////////////

static const QString &red_stylesheet()
{
	static QString q =
	    "QLabel { background-color : red; color : white;  font : 8pt; }";
	return q;
}

static const QString &green_stylesheet()
{
	static QString q =
	    "QLabel { background-color : green; color : white;  font : 9pt; }";
	return q;
}

////////////////////////////////////////////////////////////
//      class TubeStatusBar
////////////////////////////////////////////////////////////

TubeStatusBar::TubeStatusBar(QWidget *parent)
    : QStatusBar(parent), is_ready_buf_(false), interlock_set_buf_(false),
      hv_is_enabled_buf_(false), monitor_l_(nullptr), interlock_l_(nullptr),
      hv_l_(nullptr)
{
	setupUi();
}

void TubeStatusBar::setTubeReady(bool is_ready)
{
	assert(monitor_l_ && "is NULL");
	monitor_l_->setStyleSheet((is_ready) ? green_stylesheet()
					     : red_stylesheet());
}

void TubeStatusBar::setInterlock(bool is_set)
{
	assert(interlock_l_ && "is NULL");
	interlock_l_->setStyleSheet((is_set) ? green_stylesheet()
					     : red_stylesheet());
}

void TubeStatusBar::setHVEnabled(bool is_enabled)
{
	assert(hv_l_ && "is NULL");
	hv_l_->setStyleSheet((is_enabled) ? green_stylesheet()
					  : red_stylesheet());
}

void TubeStatusBar::updateStates(MiniXUpdater::Readable r)
{
	if (r.device_is_ready != is_ready_buf_) {
		setTubeReady(r.device_is_ready);
		is_ready_buf_ = r.device_is_ready;
	}
	if (r.interlock_is_enabled != interlock_set_buf_) {
		setInterlock(r.interlock_is_enabled);
		interlock_set_buf_ = r.interlock_is_enabled;
	}
	if (r.hv_is_enabled != hv_is_enabled_buf_) {
		setHVEnabled(r.hv_is_enabled);
		hv_is_enabled_buf_ = r.hv_is_enabled;
	}
}

void TubeStatusBar::tempMessage(const QString &msg)
{
	this->showMessage(msg, 5000);
}

void TubeStatusBar::setupUi()
{

	QFrame *w = new QFrame(this);
	assert(w && "is NULL");

	w->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

	QVBoxLayout *layout = new QVBoxLayout(w);
	assert(layout && "is NULL");

	// monitor
	assert(!monitor_l_ && "not NULL");
	monitor_l_ = new QLabel(w);
	assert(monitor_l_ && "is NULL");

	monitor_l_->setMinimumWidth(120);
	monitor_l_->setAlignment(Qt::AlignCenter);
	monitor_l_->setStyleSheet(red_stylesheet());
	monitor_l_->setText("monitor ready?");
	layout->addWidget(monitor_l_, 10);

	// interlock
	assert(!interlock_l_ && "not NULL");
	interlock_l_ = new QLabel(w);
	assert(interlock_l_ && "is NULL");

	interlock_l_->setMinimumWidth(120);
	interlock_l_->setAlignment(Qt::AlignCenter);
	interlock_l_->setStyleSheet(red_stylesheet());
	interlock_l_->setText("interlock set?");
	layout->addWidget(interlock_l_, 10);

	// hv
	assert(!hv_l_ && "not NULL");
	hv_l_ = new QLabel(w);
	assert(hv_l_ && "is NULL");

	hv_l_->setMinimumWidth(120);
	hv_l_->setAlignment(Qt::AlignCenter);
	hv_l_->setStyleSheet(red_stylesheet());
	hv_l_->setText("hv enabled?");
	layout->addWidget(hv_l_, 10);

	w->setLayout(layout);
	this->addPermanentWidget(w);
}

} // namespace amptek
