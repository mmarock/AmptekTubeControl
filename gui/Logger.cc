

/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "../src/Logger.hh"

#include <QMessageLogger>

#include <cstdarg>
#include <cstdio>

namespace logger
{

////////////////////////////////////////
//      static non member
////////////////////////////////////////
static struct Data {
	enum VERBOSITY current;

	QMessageLogger msg_logger;

	Data() : current(VERBOSITY::V_INFO), msg_logger() {}

	~Data() {}

} data_;

void set_verbosity(enum VERBOSITY v) { data_.current = v; }

const enum VERBOSITY &verbosity() { return data_.current; }

void write(const enum VERBOSITY &v, const char *fmt, ...)
{
	auto curr = data_.current;

	if (curr < v) {
		return;
	}

	char buf[1024];
	va_list args;

	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);

	switch (curr) {
	case V_EMERG:
		data_.msg_logger.fatal("%s", buf);
		break;
	case V_ALERT:
		data_.msg_logger.fatal("%s", buf);
		break;
	case V_CRIT:
		data_.msg_logger.critical("%s", buf);
		break;
	case V_ERR:
		data_.msg_logger.critical("%s", buf);
		break;
	case V_WARNING:
		data_.msg_logger.warning("%s", buf);
		break;
	case V_NOTICE:
		data_.msg_logger.info("%s", buf);
		break;
	case V_INFO:
		data_.msg_logger.info("%s", buf);
		break;
	case V_DEBUG:
		data_.msg_logger.debug("%s", buf);
		break;
	};
}

} // namespace logger
