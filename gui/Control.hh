

#ifndef CONTROL_HH_INCLUDED
#define CONTROL_HH_INCLUDED

#include <QWidget>

class QLabel;
class QLineEdit;
class QPushButton;

namespace amptek
{

class MiniX;
struct MiniXConfig;

class Control : public QWidget
{
	Q_OBJECT

	QPushButton *button_; ///< Toggle button
	enum class ButtonState {
		on,
		off
	} current_state_; ///< The current button state

	QLineEdit *voltage_;
	QLineEdit *current_;

public:
	explicit Control(QWidget *parent, const MiniX &dev);

public slots:
	void enableHvButton();

signals:
	void currentChanged(double current);
	void voltageChanged(double voltage);

	void hvOnPressed();
	void hvOffPressed();

private:
	QLabel *init_voltage_edit(const MiniXConfig &conf);
	QLabel *init_current_edit(const MiniXConfig &conf);

	void setupUi(const MiniX &dev);

	void saveVoltageChange();
	void saveCurrentChange();

	void toggleButton();

}; // class Control

} // namespace amptek

#endif // CONTROL_HH_INCLUDED
