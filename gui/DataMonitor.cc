/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "DataMonitor.hh"

#include <QHBoxLayout>

#include <cassert>

namespace amptek
{

/**
 * @brief constructor
 *
 * @note Derived classes need to call
 *      DataMonitor::init() since
 *      the initialization depends on some
 *      virtual functions
 *
 * @param parent Parent QWidget
 */
DataMonitor::DataMonitor(QWidget *parent)
    : QWidget(parent), data_(nullptr), initialized_(false)
{
}

/**
 * @brief Must be called by every derived class
 */
void DataMonitor::init()
{
	setupUi();
	initialized_ = true;
}

DataMonitor::~DataMonitor() {}

/**
 * @brief Set the value in the associated QLabel
 *
 * @param value
 */
void DataMonitor::setValue(double value)
{
	assert(initialized_ && "init the base class!");
	assert(data_ && "data_ not initialized");
	data_->setText(QString("%1 %2").arg(value, 0, 'd', 2).arg(unit()));
}

/**
 * @brief Init the user interface
 */
void DataMonitor::setupUi()
{
	QHBoxLayout *hlayout = new QHBoxLayout(this);
	assert(hlayout && "fucked up, is NULL");

	QLabel *l = new QLabel(this);
	assert(l && "fucked up, is NULL");
	l->setText(label());

	assert(!data_ && "data_ not NULL");
	data_ = new QLabel(this);
	assert(data_ && "fucked up, is NULL");

	data_->setAlignment(Qt::AlignLeft);
	data_->setText("nan");

	hlayout->addWidget(l, 5, Qt::AlignLeft);
	hlayout->addWidget(data_, 5, Qt::AlignLeft);
	this->setLayout(hlayout);
}

} // namespace amptek
