/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef DATAMONITOR_HH_INCLUDED
#define DATAMONITOR_HH_INCLUDED

#include <QLabel>
#include <QWidget>

namespace amptek
{

class DataMonitor : public QWidget
{
	Q_OBJECT

	QLabel *data_;

	bool initialized_;

public:
	explicit DataMonitor(QWidget *parent);
	void init();

	virtual ~DataMonitor() = 0;

signals:
	void valueUpdated(QString);

public slots:
	void setValue(double value);

private:
	void setupUi();

	virtual const char *label() = 0;
	virtual const char *unit() = 0;
};

} // namespace amptek

#endif // DATAMONITOR_HH_INCLUDED
