/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "Monitor.hh"

#include <QVBoxLayout>

#include <cassert>

#include "DataMonitor.hh"

namespace amptek
{

////////////////////////////////////////
//      class VoltageMonitor
////////////////////////////////////////

class VoltageMonitor : public DataMonitor
{
public:
	explicit VoltageMonitor(QWidget *parent) : DataMonitor(parent)
	{
		DataMonitor::init();
	}

private:
	const char *label() override { return "Voltage"; }
	const char *unit() override { return "kV"; }
};

////////////////////////////////////////
//      class CurrentMonitor
////////////////////////////////////////
class CurrentMonitor : public DataMonitor
{
public:
	explicit CurrentMonitor(QWidget *parent) : DataMonitor(parent)
	{
		DataMonitor::init();
	}

private:
	const char *label() override { return "Current"; }
	const char *unit() override { return "uA"; }
};

////////////////////////////////////////
//      class PowerMonitor
////////////////////////////////////////
class PowerMonitor : public DataMonitor
{
public:
	explicit PowerMonitor(QWidget *parent) : DataMonitor(parent)
	{
		DataMonitor::init();
	}

private:
	const char *label() override { return "Power"; }
	const char *unit() override { return "mW"; }
};

////////////////////////////////////////
//      class TemperatureMonitor
////////////////////////////////////////
class TemperatureMonitor : public DataMonitor
{
public:
	explicit TemperatureMonitor(QWidget *parent) : DataMonitor(parent)
	{
		DataMonitor::init();
	}

private:
	const char *label() override { return "Temperature"; }
	const char *unit() override { return "°C"; }
};

////////////////////////////////////////////////////////////
//      class Monitor
////////////////////////////////////////////////////////////

Monitor::Monitor(QWidget *parent)
    : QWidget(parent), voltage_(nullptr), current_(nullptr), power_(nullptr),
      temperature_(nullptr)
{
	setupUi();
}

/**
 * @brief signaled by the MiniXUpdater, updates the layout
 *
 * @param r acquired vals
 */
void Monitor::updateData(struct MiniXUpdater::Readable r)
{
	voltage_->setValue(r.voltage);
	current_->setValue(r.current);
	power_->setValue(r.power);
	temperature_->setValue(r.temperature);

	emit dataUpdated();
}

/**
 * @brief init the user interface
 *
 * One DataMonitor for each quantity
 * is created and added to the layout
 */
void Monitor::setupUi()
{
	assert(!voltage_ && "voltage not NULL");
	assert(!current_ && "current not NULL");
	assert(!power_ && "power not NULL");
	assert(!temperature_ && "temperature not NULL");

	QVBoxLayout *vlayout = new QVBoxLayout(this);
	assert(vlayout && "is NULL");

	voltage_ = new VoltageMonitor(this);
	assert(voltage_ && "is NULL");

	current_ = new CurrentMonitor(this);
	assert(current_ && "is NULL");

	power_ = new PowerMonitor(this);
	assert(power_ && "is NULL");

	temperature_ = new TemperatureMonitor(this);
	assert(temperature_ && "is NULL");

	vlayout->addWidget(voltage_);
	vlayout->addWidget(current_);
	vlayout->addWidget(power_);
	vlayout->addWidget(temperature_);

	this->setLayout(vlayout);
}

} // namespace amptek
