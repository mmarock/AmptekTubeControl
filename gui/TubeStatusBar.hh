

#ifndef TUBE_STATUS_BAR
#define TUBE_STATUS_BAR

#include <QStatusBar>

#include "MiniXUpdater.hh"

class QLabel;

namespace amptek
{

class TubeStatusBar : public QStatusBar
{

	Q_OBJECT

	bool is_ready_buf_;
	bool interlock_set_buf_;
	bool hv_is_enabled_buf_;

	QLabel *monitor_l_;
	QLabel *interlock_l_;
	QLabel *hv_l_;

public:
	explicit TubeStatusBar(QWidget *parent);

	void setTubeReady(bool is_ready);
	void setInterlock(bool is_set);
	void setHVEnabled(bool is_enabled);

public slots:
	void updateStates(struct MiniXUpdater::Readable r);
	void tempMessage(const QString &msg);

private:
	void setupUi();
};

} // namespace amptek

#endif // TUBE_STATUS_BAR
