

/* Copyright (C)
 *2018 - Marcel Marock
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include "MiniXUpdater.hh"

#include <QTimer>

#include <cassert>

#include "../src/Logger.hh"

namespace amptek
{

////////////////////////////////////////////////////////////
//      class MiniXUpdater
////////////////////////////////////////////////////////////

MiniXUpdater::MiniXUpdater(QObject *parent, MiniXDevice &&dev)
    : QObject(parent), updater_(nullptr),
      device_(std::forward<MiniXDevice>(dev)), device_failed_(false),
      do_start_(false), do_stop_(false), set_voltage_(false), voltage_buf_(0),
      set_current_(false), current_buf_(0)
{
	setup();
}

MiniXUpdater::~MiniXUpdater()
{
	try {
		device_.cleanup();
	} catch (const MiniXException &ex) {
		logger::write((device_failed_) ? logger::V_DEBUG
					       : logger::V_CRIT,
			      "MiniXWidget::~MiniXWidget(): %s", ex.what());
	}
}

/**
 * @brief get the initialized MiniX
 */
const MiniX &MiniXUpdater::getMiniX() const noexcept { return device_; }

/**
 * @brief enable the high voltage
 */
void MiniXUpdater::start() noexcept
{
	assert(updater_ && "is NULL");
	do_start_ = true;
}

/**
 * @brief disable the high voltage
 */
void MiniXUpdater::stop() noexcept
{
	assert(updater_ && "is NULL");
	do_stop_ = true;
}

/**
 * @brief instruct the timeout handler to set the current
 *
 * @param curr current value in uA
 */
void MiniXUpdater::setCurrent(double curr) noexcept
{
	if (!set_current_) {
		current_buf_ = curr;
		set_current_ = true;
	}
}

/**
 * @brief instruct the timeout handler to set the voltage
 *
 * @param voltage voltage in kV
 */
void MiniXUpdater::setVoltage(double voltage) noexcept
{
	if (!set_voltage_) {
		voltage_buf_ = voltage;
		set_voltage_ = true;
	}
}

/**
 * @brief set the updater_ and connect the timeout signal
 */
void MiniXUpdater::setup()
{
	assert(!updater_ && "not NULL");
	updater_ = new QTimer(this);
	assert(updater_ && "is NULL");
	connect(updater_, &QTimer::timeout, this, &MiniXUpdater::onTimeout);

	updater_->start(750);
}

/**
 * @brief periodically called function
 *
 * This function is called by the timer
 * and communicates exclusively with the MiniX
 * device. Boolean variables are queried and
 * applied, the received values are not buffered
 * but emitted by the MiniXUpdater::valuesUpdated signal.
 *
 * inputError and criticalError must be connected to an appropiated
 * slot, the inputError is not critical but must be reported
 */
void MiniXUpdater::onTimeout()
{
	try {

		auto state = device_.state();

		// implicit rhs object
		emit valuesUpdated({ MiniX::monitor_is_ready(state),
				     MiniX::interlock_is_enabled(state),
				     MiniX::hv_is_enabled(state), device_.hv(),
				     device_.current(), device_.wattage(),
				     device_.temperature() });

		if (do_start_) {
			device_.enableHV();
			do_start_ = false;
			updater_->setInterval(250);
		}

		if (do_stop_) {
			device_.disableHV();
			do_stop_ = false;
			updater_->setInterval(750);
		}

		if (set_voltage_) {
			device_.setHV(voltage_buf_);
			set_voltage_ = false;
		}

		if (set_current_) {
			device_.setCurrent(current_buf_);
			set_current_ = false;
		}

	} catch (const InputError &ex) {
		emit inputError(ex.what());

		set_voltage_ = false;
		set_current_ = false;
		do_start_ = false;
		// TODO: do_stop must be executed next time!

	} catch (const CriticalError &ex) {
		// critical: the device does not work anymore
		updater_->stop();
		device_failed_ = true;

		emit criticalError(ex.what());

	} catch (const MiniXException &ex) {
		logger::write(logger::V_CRIT, "MiniXUpdater::onTimeout(): %s",
			      ex.what());
		assert(0 && "check log, this is undefined!");
	}
}

} // namespace amptek
