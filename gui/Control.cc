#include "Control.hh"

#include <QDoubleValidator>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <cassert>

#include "../src/MiniX.hh"

namespace amptek
{

Control::Control(QWidget *parent, const MiniX &dev)
    : QWidget(parent), button_(nullptr), current_state_(ButtonState::off),
      voltage_(nullptr), current_(nullptr)
{
	setupUi(dev);
}

/**
 * @brief signaled by the MiniXUpdater, releases the button
 */
void Control::enableHvButton() { button_->setDown(false); }

/**
 * @brief Init the voltage QLabel and QLineEdit
 *
 * @param conf Get the default voltage values
 *
 * Initializes the voltage_ QLineEdit and a
 * QDoubleValidator
 *
 * @todo The QDoubleValidator seems to reject
 *  decimal point and I do not get, why
 *
 * @return The QLabel pointer (since the QLineEdit is saved in the object)
 */
QLabel *Control::init_voltage_edit(const MiniXConfig &conf)
{
	QLabel *voltage_l = new QLabel(this);
	assert(voltage_l && "is NULL");

	voltage_l->setText("voltage [kV]");

	assert(!voltage_ && "not NULL");
	voltage_ = new QLineEdit(this);
	assert(voltage_ && "is NULL");

	QDoubleValidator *voltage_val = new QDoubleValidator(voltage_);
	assert(voltage_val && "is NULL");

	voltage_val->setDecimals(4);
	voltage_val->setRange(conf.hv_min, conf.hv_max);
	voltage_val->setNotation(QDoubleValidator::StandardNotation);

	voltage_->setValidator(voltage_val);
	voltage_->setText(QString::number(conf.hv_default));
	return voltage_l;
}

/**
 * @brief Init the current QLabel and QLineEdit
 *
 * @param conf Get the default current values
 *
 * Initializes the current_ QLineEdit and a
 * QDoubleValidator
 *
 * @todo The QDoubleValidator seems to reject
 *  decimal point and I do not get, why
 *
 * @return The QLabel pointer (since the QLineEdit is saved in the object)
 */
QLabel *Control::init_current_edit(const MiniXConfig &conf)
{
	QLabel *current_l = new QLabel(this);
	assert(current_l && "is NULL");

	current_l->setText("current [uA]");

	assert(!current_ && "not NULL");
	current_ = new QLineEdit(this);
	assert(current_ && "is NULL");

	QDoubleValidator *current_val = new QDoubleValidator(current_);
	assert(current_val && "is NULL");

	current_val->setDecimals(4);
	current_val->setRange(conf.curr_min, conf.curr_max);
	current_val->setNotation(QDoubleValidator::StandardNotation);

	current_->setValidator(current_val);
	current_->setText(QString::number(conf.curr_default));
	return current_l;
}

/**
 * @brief init the user interface
 *
 * This consists of two labels and
 * corresponding line edits (current, voltage)
 * as well as the enable/disable HV button.
 *
 * @param dev The MiniX device, mainly for some default values
 *
 */
void Control::setupUi(const MiniX &dev)
{
	QGridLayout *gframe = new QGridLayout(this);
	assert(gframe && "is NULL");

	const auto &conf = dev.getConfig();

	QLabel *voltage_l = init_voltage_edit(conf);
	connect(voltage_, &QLineEdit::editingFinished, this,
		&Control::saveVoltageChange);

	QLabel *voltage_max_l = new QLabel(this);
	assert(voltage_max_l && "is NULL");
	voltage_max_l->setText(
	    QString("[%1,%2]").arg(conf.hv_min).arg(conf.hv_max));

	QLabel *current_l = init_current_edit(conf);
	connect(current_, &QLineEdit::editingFinished, this,
		&Control::saveCurrentChange);

	QLabel *current_max_l = new QLabel(this);
	assert(current_max_l && "is NULL");
	current_max_l->setText(
	    QString("[%1,%2]").arg(conf.curr_min).arg(conf.curr_max));

	assert(!button_ && "not NULL");
	button_ = new QPushButton(this);
	assert(button_ && "is NULL");

	button_->setText("enable HV");
	connect(button_, &QPushButton::pressed, this, &Control::toggleButton);

	gframe->addWidget(voltage_l, 0, 0, 1, 1);
	gframe->addWidget(voltage_, 0, 1, 1, 1);
	gframe->addWidget(voltage_max_l, 0, 2, 1, 1);

	gframe->addWidget(current_l, 1, 0, 1, 1);
	gframe->addWidget(current_, 1, 1, 1, 1);
	gframe->addWidget(current_max_l, 1, 2, 1, 1);

	gframe->addWidget(button_, 2, 0, 1, 2);

	this->setLayout(gframe);
}

/**
 * @brief emit voltageChanged() signal
 */
void Control::saveVoltageChange()
{
	emit voltageChanged(voltage_->text().toDouble());
}

/**
 * @brief emit currentChanged() signal
 */
void Control::saveCurrentChange()
{
	emit currentChanged(current_->text().toDouble());
}

/**
 * @brief the HV On/Off
 */
void Control::toggleButton()
{
	// lock the button, gets
	// unlocked by calling slot
	// enableHvButton()
	button_->setDown(true);

	// now toggle state and
	// emit the signal
	switch (current_state_) {
	case ButtonState::off:
		emit hvOnPressed();
		current_state_ = ButtonState::on;
		button_->setText("disable HV");
		return;
	case ButtonState::on:
		emit hvOffPressed();
		current_state_ = ButtonState::off;
		button_->setText("enable HV");
		return;
	};
}

} // namespace amptek
